# Log2timeline And WSL

## Prequisite

### Instalation
- Kali
- Ubuntu
- Dockor - (My Preferense)

### Kali

```bash
sudo apt-get install plaso
```

### Ubuntu

To install Plaso from the GIFT Personal Package Archive (PPA)

```bash
sudo add-apt-repository ppa:gift/stable

```
**Update and install Plaso:**

```bash
sudo apt-get update
sudo apt-get install plaso-tools
```
### Docker

```bash
docker pull log2timeline/plaso
```
**Running Docker**

```bash
docker run -t -i --entrypoint=/bin/bash -v /data<File Location of Host Machine>:/data log2timeline/plaso
```

## Converting Vhdx File to Raw Image

log2timeline require raw image file. To convert vhdx to raw image we would need to install qemu:

**To install qemu**
```bash
sudo apt install qemu-utils
```
**Converting vhdx to raw:**
```bash
qemu-img convert -O raw 2021-02-02T073114_win10.vhdx win10_demo.raw
```

## Creating Plaso Dump file

```bash
log2timeline.py plaso.dump win10_demo.raw
```
**Plaso Dump file information**

```bash
pinfo.py plaso.dump
```
## Convert plaso file into CSV

**To list out all time zone**

```bash
psort.py --output-time-zone list
```

**Set time zone**

```bash
psort.py -o l2tcsv -w timeline.csv --output-time-zone Asia/Kolkata plaso.dump
```
**Using date filter**

```bash
psort.py -o l2tcsv -w timeline.csv --output-time-zone Asia/Kolkata plaso.dump "date > '2021-01-01' AND date < '2021-01-05'"

```

### Referrence 

- [Plaso Documentation](https://plaso.readthedocs.io/en/latest/index.html)
- [Richard Davis](https://twitter.com/davisrichardg)
